# soal-shift-sisop-modul-2-ITA04-2022

## Laporan Pengerjaan Modul 2 Praktikum Sistem Operasi

Nama Anggota Kelompok

1. ANISAH FARAH FADHILAH
2. GILANG BAYU GUMANTARA
3. M. FERNANDO N. SIBARANI

---

## Soal 1

### Soal

Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.

a. Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

b. Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

c. Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12*gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha*{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

d. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

e. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

### Cara Pengerjaan

Pertama-tama kami membuat folder 'gacha-gacha', -p digunakan untuk menghiraukan error yang ada ketika folder yang ingin dibuat itu sudah ada.

```c
char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
        jalankan("/bin/mkdir", argv);
```

Kemudian kami membuat fungsi download yang berfungsi untuk mendownload file yang dibutuhkan seperti characters.zip dan weapons.zip. Kami menggunakan wget untuk melakukan download dari url google drive dan memasukkannya ke dalam folder characters dan folder weapons.

Setelah mendownload dan mendapatkan file zip, program akan melakukan unzip terhadap file zip yang telah didownload. Hasil dari unzip tersebut dimasukkan ke dalam folder weapons dan characters.

```c
void download()
{
    int status = 0;
    // membuat  proses baru

    char *argv1[][5] = {
        {"wget",  "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL},
        {"wget", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "characters.zip", NULL}};

    // melakukan download
    for (int i = 0; i < 2; i++)
        jalankan("/usr/bin/wget", argv1[i]);
    sleep(30);

    // Unzip setelah melakukan download
    char *argv2[][6] = {
        {"unzip", "-j", "weapons.zip", "-d", "./weapons", NULL},
        {"unzip", "-j", "characters.zip", "-d", "./characters", NULL}
    };

    for (int i = 0; i < 2; i++)
        jalankan("/bin/unzip", argv2[i]);
    sleep(30);
}
```

Kami juga membuat fungsi yang bernama jumlahFile, untuk mendapatkan jumlah file yang ada dalam satu folder. Fungsi ini akan digunakan ketika akan memilih file secara acak.

```c
int jumlahFile(char path[]){
    int jumlah_file =0;
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strstr(ep->d_name, ".json")) jumlah_file++;
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    return jumlah_file;
}
```

Kemudian terdapat fungsi randomFile yang berfungsi untuk mengambil file secara acak. Fungsi ini bekerja dengan cara mengambil informasi jumlah file dari fungsi jumlahFile yang sudah dijelaskan, kemudian mengambil bilangan random yang di mod dengan jumlah file yang telah diambil. Hal ini akan menghasilkan bilangan random yang nilai terbesarnya sesuai dengan jumlah file yang ada.

Setelah itu, program akan melakukan iterasi sebanyak bilangan acak yang telah diambil. Ketika file yang diambil adalah file yang bukan json, maka program akan melakukan iterasi lagi sampai mendapat file .json. Setelah mendapat file .json secara acak, fungsi ini akan mereturn nama file tersebut.

```c
char* randomFile(char path[]){
    int jumlah_file= jumlahFile(path);
    int random_value= rand()%jumlah_file;
    DIR *dp;
    struct dirent *ep;

    dp = opendir(path);

    if (dp != NULL)
    {
        int i=0;
        while ((ep = readdir (dp))) {
            if(i>=random_value-1 && strstr(ep->d_name, ".json")) return ep->d_name;
            i++;
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}
```

Kemudian ada juga fungsi getData yang fungsinya untuk mengambil data dari json yang sudah dipilih secara acak dari fungsi randomFile. Fungsi ini mengambil data dari file .json tersebut dengan cara parsing json. Karena pada soal data yang diambil adalah data nama dan rarity, maka data yang diambil dari file json adalah nama dan rarity dengan format yang sudah ditentukan pada soal modul.

```c
char* getData(char path[]){
    FILE *fp;
    char buffer[4096];
    char *output = malloc (sizeof (char) * 1024);
    struct json_object *parsed_json;
	  struct json_object *name;
    struct json_object *rarity;

    char filename[100];
    snprintf(filename, 100, "%s/%s", path, randomFile(path));

    fp = fopen(filename, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    snprintf(output, 100, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
    return output;
}
```

Kemudian terdapat fungsi gatcha. Fungsi ini berguna untuk melakukan gatcha seperti yang dijelaskan pada soal modul. Fungsi ini akan melakukan iterasi sampai primogemsnya kurang dari harga gatcha yang telah ditentukan oleh soal.

Selama iterasi, fungsi ini akan mengecek jumlah gatcha yang telah dilakukan. Ketika jumlah gatcha mod 90 -1 sama dengan 0, maka program akan membuat folder baru dengan nama seperti yang ada pada soal modul. Ketika jumlah gatcha mod 10 - 1 sama dengan 0, maka program akan membuat file baru untuk penulisan hasil gatcha. Ketika jumlah gatcha adalah genap (sisa bagi 2 adalah 0), maka program akan mengambil data dari folder weapons. Namun ketika ganjil, maka program akan mengambil data dari folder characters.

Kemudian terdapat chdir("..") agar direktori kembali seperti semula setelah menjalankan fungsi ini.

```c
void gatcha(){
    char waktu[30], filename[100], foldername[100], message[100];
    int jumlah_gatcha = 1;
    int current_primogems = primogems_start;

    FILE * fptr;
    while(current_primogems > harga_gatcha){
    current_primogems-=harga_gatcha;
        if((jumlah_gatcha%90-1)==0){
            if(jumlah_gatcha > 1) chdir("..");
            snprintf(foldername, 100, "total_gatcha_%d", jumlah_gatcha);

            char *argv[] = {"mkdir", "-p", foldername, NULL};
            jalankan("/bin/mkdir", argv);
            chdir(foldername);
        }

        if((jumlah_gatcha%10-1)==0 || jumlah_gatcha==1){
            sleep(1);
            time_t t1 = time(NULL);
            struct tm* p1 = localtime(&t1);

            strftime(waktu, 30, "%H:%M:%S", p1);
            snprintf(filename, 100, "%s_gatcha_%d.txt", waktu, jumlah_gatcha);
        }

        if(jumlah_gatcha%2==0){
            fptr = fopen(filename, "a+");
            snprintf(message, 100, "%d_weapon_%s_%d\n", jumlah_gatcha, getData("../../weapons"), current_primogems);
            fputs(message, fptr);
            fclose(fptr);
        }else{
            fptr = fopen(filename, "a+");
            snprintf(message, 100, "%d_character_%s_%d\n", jumlah_gatcha, getData("../../characters"), current_primogems);
            fputs(message, fptr);
            fclose(fptr);
        }
        jumlah_gatcha++;
    }
    chdir("..");
}
```

Kemudian kami membuat fungsi sembunyikan untuk melakukan zip terhadap folder gatcha_gatcha dengan nama not_safe_for_wibu.zip dan password juga sesuai dengan soal yaitu 'satuduatiga'. Setelah itu, program akan menghapus file weapons.zip dan characters.zip, folder characters dan weapons.

```c
void sembunyikan(){
    char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
    jalankan("/bin/zip", argv);

    char *argv2[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
    jalankan("/bin/rm", argv2);

    char *argv3[] = {"rm", "characters.zip", "weapons.zip", NULL};
    jalankan("/bin/rm", argv3);
}
```

Kemudian terdapat fungsi makedaemon yang yaitu template daemon yang didapat dari modul.

```c
void makedaemon()
{
    pid_t pid, sid;

    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/ubuntu/praktikum_sisop/soal-shift-sisop-modul-2-ita04-2022/soal1")) < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}
```

Kemudian pada fungsi main, kami memanggil fungsi-fungsi yang telah dibuat. srand(time(NULL)) digunakan agar fungsi rand menghasilkan angka yang random. Kemudian kami membuat folder gatcha_gatcha yang dimana akan digunakan untuk memasukkan hasil gatcha. Kemudian fungsi download dipanggil untuk melakukan download file zip. Setelah itu direktori diarahkan ke folder gatcha-gatcha yang telah dibuat. Kemudian fungsi gatcha dipanggil untuk melakukan gatcha dan memasukkan hasilnya ke folder gatcha-gatcha.

Setelah selesai menjalankan fungsi dari gatcha, maka direktori diarahkan keluar dari folder gatcha. Kemudian pada soal terdapat perintah untuk melakukan zip dan menghapus file lain seperti yang ada pada fungsi sembunyikan() dijalankan setelah 3 jam. Karena itu, sebelum menjalankan fungsi sembunyikan(), dijalankan fungsi sleep dengan parameter 3600\*3.

```c
int main()
{
    // makedaemon();
    while (1)
    {
        srand(time(NULL));

        char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
        jalankan("/bin/mkdir", argv);

        download();
        chdir("gacha_gacha");
        gatcha();
        chdir("..");
        sleep(3600*3);
        sembunyikan();
        break;
    }
    return 0;
}
```

### Screenshot

![folder hasil](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal1/Folder%20Gatcha.png)

![Folder Gatcha](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal1/Folder%20Gatcha.png)

![Require Password](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal1/Require%20Password.png)

![Hasil Gatcha](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal1/Hasil%20Gatcha.png)

### Kendala yang Dihadapi

Kami mengalami kendala pada saat menjalankan program dengan daemon, program tidak berjalan dengan baik. Akan tetapi, pada saat program dijalankan seperti biasa, program dapat bekerja dengan baik.

## Soal 2

### Soal

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

    kategori : romance

    nama : start-up
    rilis  : tahun 2020

    nama : twenty-one-twenty-five
    rilis  : tahun 2022

Note dan Ketentuan Soal:

- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(\_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.

### Cara Pengerjaan

Kami banyak menggunakan `fork()` untuk menjalankan fungsi `execv()`.
Dengan adanya fungsi `wait()` di _parent process_, program akan menjalankan _child process_ terlebih dahulu. Setelah _child process_ selesai, akan menjalankan _parent process_ yang di dalamnya dibuat _child process_ baru.
Urutan proses yang dijalankan dahulu:

- _Child_ 1
- _Parent_ 1 (_child_ 2)
- _Parent_ 1 (_parent_ 2 (_child_ 3))
- _Parent_ 1 (_parent_ 2 (_parent_ 3 (_child_ 4)))
- dst

```c
a1 = fork();
if(a1 == 0){
  // child 1 (mkdir)
} else{
  while((wait(&status)) > 0);
  a2 = fork();
  if(a2 == 0){
    // child 2 (unzip)
  } else{
    while((wait(&status)) > 0);
    a3 = fork();
    if(a3 == 0){
      // child 3 (delete)
    } else{
      while((wait(&status)) > 0);
      b1 = fork();
      if(b1 == 0){
      // ... child 4 (soal b), dst
      }
    }
  }
}
```

#### **2a**

```c
char path[] = "/home/ubuntu/shift2/drakor";
```

_Child_ 1 membuat folder shift2/drakor menggunakan `mkdir` dengan parameter `-p` adalah path.

```c
char *argv[] = {"mkdir", "-p", path, NULL};
execv("/usr/bin/mkdir", argv); // buat folder shift2/drakor
```

_Child_ 2 unzip drakor.zip ke dalam folder drakor dengan parameter `-q` agar tidak menampilkan proses _unzipping_ dan `-d` adalah _directory path_.

```c
char *argv[] = {"unzip",  "-q", "drakor.zip", "-d", path, NULL};
execv("/usr/bin/unzip", argv); // unzip drakor.zip
```

_Child_ 3 menghapus folder dan file yang tidak penting (selain file png) menggunakan `find` dengan parameter `-not` `-name` `*.png` (mencari/menemukan semua (file dan folder) yang namanya tidak mengandung ".png"), kemudian dihapus (parameter `-delete`).

```c
char *argv[] = {"find", path, "-not", "-name", "*.png", "-delete", NULL};
execv("/usr/bin/find", argv); // hapus semua yang namanya tidak mengandung ".png"
```

#### **2b**

Untuk mendapatkan list isi directory dan mengolah nama-nama file, kami menggunakan fungsi `readdir()` yang kemudian disimpan ke dalam file txt dan file txt tersebut diolah menggunakan `awk`. Karena file txt yang akan dibuat cukup banyak, dibuat folder `temp` yang akan menyimpan file-file tersebut agar mudah dihapus pada akhir program.

```c
char *argv[] = {"mkdir", "-p", "temp", NULL};
execv("/usr/bin/mkdir", argv); // buat folder temp untuk menyimpan hasil txt (nanti akan dihapus)
```

Pertama, mendapatkan list nama file yang terdapat dalam folder `/home/ubuntu/shift2/drakor` (path) dengan membaca isi folder menggunakan fungsi `readdir()`, kemudian menyimpan hasil `readdir()` ke dalam file `1list_file.txt`.

```c
FILE *fp;
DIR *dp;
struct dirent *ep;
dp = opendir(path);
while ((ep = readdir(dp))){
  fp = fopen("temp/1list_file.txt", "a");
  fprintf(fp, "%s\n", ep->d_name); // simpan list nama file utk diolah menggunakan awk
  fclose(fp);
}
(void) closedir(dp);
```

List file tersebut diolah menggunakan `awk` dengan separator `;` dan mencetak `$NF` kolom/field terakhir (kategori) ke dalam file `2kategori_png.txt`. (cth: action.png)

```c
char *argv[] = {"awk", "-F", "[;]", "{print $NF > \"temp/2kategori_png.txt\"}", "temp/1list_file.txt", NULL};
execv("/usr/bin/awk", argv); // ambill [kategori].png
```

Setelah itu, diproses kembali menggunakan `awk` untuk mendapatkan nama kategori yang siap untuk dibuat folder. Untuk menghilangkan ".png", digunakan separator `.` dan memilih `$1` (kategori). Karena pada file yang diolah berisi beberapa kategori yang sama (cth: ada 2 action.png), `awk` akan menyimpan 1 kali untuk setiap kategori dalam `d_name[$1]` dan menghitung kategori tersebut ada berapa banyak. Di akhir proses `END`, akan mencetak setiap kategori ke dalam file `3kategori.txt`

```c
char *argv[] = {"awk", "-F", "[.]", "{d_name[$1]++} END{for(name in d_name) print name > \"temp/3kategori.txt\"}", "temp/2kategori_png.txt", NULL};
execv("/usr/bin/awk", argv); // ambil kategori
```

Terakhir, membuat folder berdasarkan list yang terdapat di file `3kategori.txt` (menggunakan fungsi `fgets()`) ke dalam folder drakor. Fungsi `sprintf()` digunakan untuk menyambungkan nama path `/home/ubuntu/shift2/drakor` dan nama dir `action`, `comedy`, dll. Fungsi `strtok()` digunakan untuk menghilangkan `\n` pada hasil `sprintf()` (jika tidak, folder yang dibuat dengan mkdir memiliki \n dan tidak bisa dimodifikasi (dimasukkan file) menggunakan `execv`) (`strtok()` digunakan pada hasil `sprintf()` yang digunakan dalam `execv`).

Untuk membuat folder, menggunakan `mkdir` dengan `execv` dan diperlukan membuat `fork()` dengan _parent process_ hanya berisi fungsi `wait()` agar program dapat melanjutkan proses selanjutnya (tidak keluar program setelah menjalankan `execv`).

```c
char foldername[60], dir[10];
FILE *file_dir ;
file_dir = fopen("temp/3kategori.txt", "r");
while(fgets(dir, 10, file_dir) != NULL){
  snprintf(foldername, 60, "%s/%s", path, dir);
  strtok(foldername, "\n");
  b3 = fork();
  if(b3 == 0){
    char *argv[] = {"mkdir", "-p", foldername, NULL};
    execv("/usr/bin/mkdir", argv); // buat folder sesuai nama yg ada dlm file txt
  } else  while((wait(&status)) > 0);
}
fclose(file_dir);
```

#### **2c dan 2d**

Karena file yang berisi 2 foto memiliki genre/kategori yang berbeda, kami menyalin dan mengubah nama file yang berisi 2 foto dahulu. Jika setiap file foto sudah memiliki satu kategori (pada nama file), baru kami pindahkan ke folder masing-masing sesuai kategori. Setelah itu, mengambil list setiap folder kategori unatuk diolah pada jawaban 2e. Kemudian, baru mengubah nama file menjadi judul saja.

Pertama, ambil list file yang berisi 2 foto menggunakan `awk` dengan separator `_`.

```c
char *argv[] = {"awk", "/_/ {print > \"temp/4png_double.txt\"}", "temp/1list_file.txt", NULL};
execv("/usr/bin/awk", argv); // ambil file yang berisi 2 foto
```

Dari list tersebut, dipisahkan nama file untuk foto pertama `$1` dan foto kedua `$2`.

```c
char *argv[] = {"awk", "-F", "[_]", "{print $1\".png\" > \"temp/5png1.txt\"}", "temp/4png_double.txt", NULL};
execv("/usr/bin/awk", argv); // ambil nama file pertama
```

```c
char *argv[] = {"awk", "-F", "[_]", "{print $2 > \"temp/6png2.txt\"}", "temp/4png_double.txt", NULL};
execv("/usr/bin/awk", argv); // ambil nama file kedua
```

Setelah itu, salin file yang berisi 2 foto menggunakan `cp` dan diberi nama file kedua. File asli diubah namanya menggunakan `mv`, yaitu memindahkan dari tempat yang sama ke tempat yang sama pula dengan nama file pertama. Untuk mengambil nama file pertama dan kedua dalam file txt, menggunakan fungsi `fgets()`.

```c
FILE *f_png_double, *f_png2, *f_png1;
char filename[90], filecopy[70], filepng1[70], pngname[60], png2[40], png1[40];
f_png_double = fopen("temp/4png_double.txt", "r");
f_png1 = fopen("temp/5png1.txt", "r");
f_png2 = fopen("temp/6png2.txt", "r");
while(fgets(pngname, 60, f_png_double) != NULL){
  snprintf(filename, 90, "%s/%s", path, pngname); // /home/ubuntu/shift2/drakor/start-up;2020;romance_the-k2;2016;action.png
  strtok(filename, "\n");
  fgets(png2, 40, f_png2);
  snprintf(filecopy, 70, "%s/%s", path, png2); // /home/ubuntu/shift2/drakor/the-k2;2016;action.png
  strtok(filecopy, "\n");
  fgets(png1, 40, f_png1);
  snprintf(filepng1, 70, "%s/%s", path, png1); // /home/ubuntu/shift2/drakor/start-up;2020;romance.png
  strtok(filepng1, "\n");
  d4 = fork();
  if(d4 == 0){
    char *argv[] = {"cp", filename, filecopy, NULL};
    execv("/usr/bin/cp", argv);
  } else{
    while((wait(&status)) > 0);
    d5 = fork();
    if(d5 == 0){
      char *argv[] = {"mv", filename, filepng1, NULL};
      execv("/usr/bin/mv", argv);
    } else  while((wait(&status)) > 0);
  }
}
fclose(f_png_double);
fclose(f_png1);
fclose(f_png2);
```

Setelah setiap file memiliki nama file dengan 1 kategori, barulah file-file tersebut dipindahkan ke folder masing-masing. Dengan fungsi `readdir()`, akan dibaca isi dari folder drakor. Jika menemukan nama file foto (png) yang mengandung nama kategori (dicocokkan/dibandingkan menggunakan fungsi `strstr()`), akan dipindah ke folder kategori tersebut.

```c
char filemove[290], fmove[300];
DIR *_dp;
struct dirent *_ep;
_dp = opendir(path);
while((_ep = readdir(_dp))){
  snprintf(filemove, 290, "%s/%s", path, _ep->d_name);
  strtok(filemove, "\n"); // filemove : /home/ubuntu/shift2/drakor/forecasting-love-and-weather;2022;romance.png
  if(strstr(_ep->d_name, ".png")){
    if      (strstr(_ep->d_name, "action"))     snprintf(fmove, 300, "%s/action/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "comedy"))     snprintf(fmove, 300, "%s/comedy/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "fantasy"))    snprintf(fmove, 300, "%s/fantasy/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "horror"))     snprintf(fmove, 300, "%s/horror/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "romance"))    snprintf(fmove, 300, "%s/romance/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "school"))     snprintf(fmove, 300, "%s/school/%s", path, _ep->d_name);
    else if (strstr(_ep->d_name, "thriller"))   snprintf(fmove, 300, "%s/thriller/%s", path, _ep->d_name);
    strtok(fmove, "\n"); // fmove : /home/ubuntu/shift2/drakor/romance/forecasting-love-and-weather;2022;romance.png
    c1 = fork();
    if(c1 == 0){
      char *argv[] = {"mv", filemove, fmove, NULL};
      execv("/usr/bin/mv", argv);
    } else  while((wait(&status)) > 0);
  }
}
(void) closedir(_dp);
```

Setelah itu, ambil list file (nama file masih mengandung tahun rilis dan kategori) dalam setiap folder menggunakan fungsi `getlist()` dengan parameter kategori (action, comedy, dll). Hasilnya akan diolah untuk mendapatkan judul.

```c
int getlist(char *kategori){
  FILE *file_kategori;
  DIR *dp_kategori;
  struct dirent *ep_kategori;
  char path[] = "/home/ubuntu/shift2/drakor";
  char dirpath[40], txtfile[20];
  snprintf(dirpath, 40, "%s/%s", path, kategori);
  dp_kategori = opendir(dirpath);
  while ((ep_kategori = readdir (dp_kategori))){
    snprintf(txtfile, 20, "temp/%s.txt", kategori); // action.txt
    file_kategori = fopen(txtfile, "a");
    fprintf(file_kategori, "%s\n", ep_kategori->d_name); // simpan list nama file utk diolah menggunakan awk
    fclose(file_kategori);
  }
  (void) closedir(dp_kategori);
}
```

```c
getlist("action");
getlist("comedy");
getlist("fantasy");
getlist("horror");
getlist("romance");
getlist("school");
getlist("thriller");
```

Semua file [kategori].txt tersebut diolah menggunakan `awk` dan `sort` untuk mendapatkan judul yang akan digunakan sebagai nama file.

`sort` digunakan untuk mempermudah pengerjaan 2e (diurut berdasarkan tahun rilis), parameter `-t` adalah separator (`;`), `-nk2` adalah `-n` yang diurutkan berupa angka dan `-k2` berada di kolom 2, `-o` adalah _output_ disimpan dalam file `[kategori]_sort.txt`

Contoh untuk kategori action:

```c
char *argv[] = {"awk", "/png/ {print $1 > \"temp/action_png.txt\"}", "temp/action.txt", NULL};
execv("/usr/bin/awk", argv); // ambil nama film action (.png)
```

```c
char *argv[] = {"sort", "-t", ";", "-nk2", "temp/action_png.txt", "-o", "temp/action_sort.txt", NULL};
execv("/usr/bin/sort", argv); // sort action
```

```c
char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/action_name.txt\"}", "temp/action_sort.txt", NULL};
execv("/usr/bin/awk", argv); // ambil nama film action
```

Setelah semua kategori selesai, barulah file-file dalam setiap kategori di-_rename_ menjadi judul menggunakan fungsi `renamefile()` dengan parameter kategori (action, comedy, dll).

```c
int renamefile(char *kategori){
  pid_t pid;
  int status;
  FILE *f_kategori, *f_name;
  char dirpath[40], file_kategori[25], file_name[25], filename[100], png[50], name[40], filerename[100];
  snprintf(dirpath, 40, "/home/ubuntu/shift2/drakor/%s", kategori); // /home/ubuntu/shift2/drakor/action
  snprintf(file_kategori, 25, "temp/%s_sort.txt", kategori); // temp/action_sort.txt
  snprintf(file_name, 25, "temp/%s_name.txt", kategori); // temp/action_name.txt
  f_kategori = fopen(file_kategori, "r");
  f_name = fopen(file_name, "r");
  while(fgets(png, 50, f_kategori) != NULL){
    snprintf(filename, 100, "%s/%s", dirpath, png); // /home/ubuntu/shift2/drakor/the-k2;2016;action.png
    strtok(filename, "\n");
    fgets(name, 50, f_name);
    snprintf(filerename, 100, "%s/%s", dirpath, name); // /home/ubuntu/shift2/drakor/the-k2.png
    strtok(filerename, "\n");
    pid = fork();
    if(pid == 0){
      char *argv[] = {"mv", filename, filerename, NULL};
      execv("/usr/bin/mv", argv);
    } else while((wait(&status)) > 0);
  }
  fclose(f_kategori);
  fclose(f_name);
}
```

```c
renamefile("action");
renamefile("comedy");
renamefile("fantasy");
renamefile("horror");
renamefile("romance");
renamefile("school");
renamefile("thriller");
```

#### **2e**

File `[kategori]_sort.txt` berisi `judul;tahun;...` yang sudah diurutkan berdasarkan tahun. Dengan ini, pengamilan judul pada file `[kategori]_name_.txt` untuk me-_rename_ file pada 2c dan 2d juga sudah diambil urut berdasarkan tahun. Untuk 2e ini, hanya perlu mengambil tahun rilis saja dari file _sort_. Sehingga, pada judul dalam file `_name` baris pertama akan sesuai dengan tahun rilisnya dalam file `_year` yang nantinya akan dicetak dalam `data.txt`.

Contoh pengambilan tahun rilis kategori action:

```c
char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/action_year.txt\"}", "temp/action_sort.txt", NULL};
execv("/usr/bin/awk", argv); // ambil tahun film action
```

Setelah dilakukan untuk semua kategori, barulah membuat `data.txt` dan mencetak/mengisi sesuai format menggunakan fungsi `makedata()`.

```c
int makedata(char *kategori){
  char path[] = "/home/ubuntu/shift2/drakor";
  char f_data[50], path_name[25], path_year[25], n[35], y[8];
  FILE *data, *name, *year;
  snprintf(f_data, 50, "%s/%s/data.txt", path, kategori); // /home/ubuntu/shift2/drakor/action/data.txt
  snprintf(path_name, 25, "temp/%s_name.txt", kategori); // temp/action_name.txt
  snprintf(path_year, 25, "temp/%s_year.txt", kategori); // temp/action_year.txt
  name = fopen(path_name, "r"); //forecasting-love-and-weather;2022;romance
  year = fopen(path_year, "r");
  data = fopen(f_data, "w");
  fprintf(data, "kategori : %s\n", kategori);
  while(fgets(n, 35, name) != NULL){
    if(fgets(y, 8, year) != NULL){
      fprintf(data, "\nnama : %srilis : %s", n, y);
    }
  }
  fclose(data);
  fclose(name);
  fclose(year);
}
```

Dalam perintah

```c
while(fgets(n, 35, name) != NULL){
  if(fgets(y, 8, year) != NULL){
    fprintf(data, "\nnama : %srilis : %s", n, y);
  }
}
```

akan mencetak isi file `name` baris pertama dan isi file `year` baris pertama. Selanjutnya mencetak isi file `name` baris kedua dan isi file `year` baris kedua, dst.

Terakhir, setelah selesai, folder `temp` yang berisi semua file txt dihapus menggunakan `rm` dengan parameter `-r` _recursive_ (menghapus folder dan seisinya)

```c
char *argv[] = {"rm", "-r", "temp", NULL};
execv("/usr/bin/rm", argv); // hapus folder temp
```

### Screenshot

List isi folder `/home/ubuntu/shift2/drakor`

![isi_folder_drakor](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/isi_folder_drakor.jpg)

![folder_kategori](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/folder_kategori.jpg)

Hasil kategori action

![action](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/action.jpg)

Hasil kategori comedy

![comedy](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/comedy.jpg)

Hasil kategori fantasy

![fantasy](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/fantasy.jpg)

Hasil kategori horror

![horror](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/horror.jpg)

Hasil kategori romance

![romance](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/romance.jpg)

Hasil kategori school

![school](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/school.jpg)

Hasil kategori thriller

![thriller](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal2/thriller.jpg)

### Kendala yang dihadapi

1. Error saat menjalankan `execv` dengan parameter adalah hasil dari `sprintf()` (solusi menggunakan `strtok()` untuk menghilangkan `\n` dari hasil `sprintf()`)
2. Terkadang setelah beberapa kali program dijalankan (diuji berkali-kali), laptop "nge-_freeze_" dan _virtual machine_ menutup sendiri karena memori terlalu penuh dialokasikan untuk _parent process_ yang tidak ada isinya (`fork()` di dalam `while()`).

## SOAL

3. Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan
   tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang
   banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2
directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian
membuat directory ke 2 dengan nama “air”.

b. Kemudian program diminta dapat melakukan extract “animal.zip” di
“/home/[USER]/modul2/”.

c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai
dengan nama filenya. Untuk hewan darat dimasukkan ke folder
“/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder
“/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder
air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang
tidak ada keterangan air atau darat harus dihapus.

d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air.
Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak
kebun binatang harus merelakannya sehingga conan harus menghapus semua
burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung
ditandai dengan adanya “bird” pada nama file.

e. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan
membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air”
ke “list.txt” dengan format UID\_[UID file permission]\_Nama File.[jpg/png] dimana
UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

## Cara Pengerjaan

Pertama-tama membuat fungsi jalankan

void jalankan(char *perintah, char*argv[]){
int status=0;
if(fork()==0)
execv(perintah, argv);
while(wait(&status)>0);
}
execv apabila menjalankan child process
dan while wait supaya paren menunggu perintahnya dijalankan

Setelah itu membuat directory dengan nama "darat" kemudian
3 detik setelah itu membuat directory kedua dengan nama "air"

int main()
{  
 // int getlogin_r(char \*buf, size_t bufsize);

    char *argv[] = {"mkdir", "-p", "/home/ubuntu/modul2/", NULL};
    jalankan("/bin/mkdir", argv);

    char *argv1[] = {"mkdir", "-p", "/home/ubuntu/modul2/darat", NULL};
    jalankan("/bin/mkdir", argv1);
    sleep(3);

    char *argv2[] = {"mkdir", "-p", "/home/ubuntu/modul2/air", NULL};
    jalankan("/bin/mkdir", argv2);

Kemuadian mengekstrak folder "animal.zip"

    char *argv3[] = {"unzip", "-j", "/home/ubuntu/modul2/animal.zip", "-d", "/home/ubuntu/modul2/", NULL};
    jalankan("/bin/unzip", argv3);

Setelah itu hasil ekstrak dimasukkan ke dalam folder yang telah dibuat
sesuai dengan nama filenya, dan hewan yang tidak ada keterangan air atau
darat dihapus

DIR *dp;
struct dirent *ep;

    dp = opendir("/home/ubuntu/modul2/");

deklarasi untuk membuka direktori

char filename[100];
if (dp != NULL)
{
while ((ep = readdir (dp))) {
if (strstr(ep->d_name, ".jpg")){
snprintf(filename, 100, "/home/ubuntu/modul2/%s", ep->d_name);

program untuk membaca semua folder yang ada di modul 2

if (strstr(ep->d_name, "darat")){
char *argv6[] = {"mv", filename, "/home/ubuntu/modul2/darat/", NULL};
jalankan("/bin/mv", argv6);
}else if (strstr(ep->d_name, "air")){
char *argv6[] = {"mv", filename, "/home/ubuntu/modul2/air/", NULL};
jalankan("/bin/mv", argv6);
}

memisahkan file sesuai kategori

else{
char \*argv6[] = {"rm", filename, NULL};
jalankan("/bin/rm", argv6);
}

menghapus file yang tidak ada keterangan

(void) closedir (dp);
} else perror ("Couldn't open the directory");

deklarasi menutup direktori dan apabila terjadi error

Setelah itu menghapus file "burung" yang ada pada folder "darat"

DIR *dp2;
struct dirent *ep2;

    dp2 = opendir("/home/ubuntu/modul2/darat/");

    if (dp2 != NULL)
    {
        while ((ep2 = readdir(dp2))) {
            if (strstr(ep2->d_name, "bird")){
                snprintf(filename, 100, "/home/ubuntu/modul2/darat/%s", ep2->d_name);
                char *argv7[] = {"rm", filename, NULL};
                jalankan("/bin/rm", argv7);
            }
        }
        (void) closedir (dp2);
    } else perror ("Couldn't open the directory");

Terakhir membuat file "list.txt" dalam folder "air" yang berisi list
nama hewan yang ada pada folder "air"

struct passwd \*p;
uid_t uid;

    if ((p = getpwuid(uid = getuid())) == NULL) perror("getpwuid() error");

    char userid[10];
    sprintf(userid, "%s_", p->pw_name);
    DIR *dp3;
    FILE * fptr;
    struct stat info;
    struct dirent *ep3;
    struct stat fs;
    int r;

Program format nama file dalam "list.txt"

dp3 = opendir("/home/ubuntu/modul2/air/");

    fptr = fopen("/home/ubuntu/modul2/air/list.txt", "a+");
    if (dp3 != NULL)
    {
        while ((ep3 = readdir (dp3))) {
            snprintf(filename, 100, "/home/ubuntu/modul2/air/%s", ep3->d_name);

            r = stat(filename,&fs);
            if( r==-1 )
            {
                fprintf(stderr,"File error\n");
                exit(1);
            }

            if (strstr(ep3->d_name, ".jpg")){

                fputs(userid, fptr);

                if( S_ISREG(fs.st_mode) ) fputs("d", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IRUSR ) fputs("r", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IWUSR ) fputs("w", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IXUSR ) fputs("e", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IRGRP ) fputs("r", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IWGRP ) fputs("w", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IXGRP ) fputs("e", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IROTH ) fputs("r", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IWOTH ) fputs("w", fptr);
                else fputs("-", fptr);

                if( fs.st_mode & S_IXOTH ) fputs("e", fptr);
                else fputs("-", fptr);

                fputs(" ", fptr);
                fputs(ep3->d_name, fptr);
                fputs("\n", fptr);
            }
        }
        fclose(fptr);
        (void) closedir (dp3);
    } else perror ("Couldn't open the directory");

    return 0;

}

## Screenshot

![air](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal3/Air.png)
![darat](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal3/Darat.png)
![folder](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal3/Folder.png)
![run](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal3/Run.png)
![list](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-2-ita04-2022/Soal3/list.txt.png)

## Kendala yang dihadapi##

Terdapat kesalahan minor dalam pembuatan program, sehingga file "list.txt"
tidak muncul saat program dijalankan
