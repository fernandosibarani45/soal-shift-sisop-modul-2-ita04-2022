#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <json-c/json.h>
#include <time.h>
#include <dirent.h>
#include <string.h>

#define primogems_start 79000
#define harga_gatcha 160

void jalankan(char *perintah, char*argv[]){
    int status=0;
    if(fork()==0)
        execv(perintah, argv);
    while(wait(&status)>0);
}

void download()
{
    int status = 0;
    // membuat  proses baru

    char *argv1[][6] = {
        {"wget", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "--no-check-certificate", "-O", "weapons.zip", NULL},
        {"wget", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "--no-check-certificate", "-O", "characters.zip", NULL}};

    // melakukan download
    for (int i = 0; i < 2; i++)
        jalankan("/usr/bin/wget", argv1[i]);

    // Unzip setelah melakukan download
    char *argv2[][6] = {
        {"unzip", "-j", "./weapons.zip", "-d", "./weapons", NULL},
        {"unzip", "-j", "./characters.zip", "-d", "./characters", NULL}
    };

    for (int i = 0; i < 2; i++)
        jalankan("/bin/unzip", argv2[i]);
}

int jumlahFile(char path[]){
    int jumlah_file =0;
    DIR *dp;
    struct dirent *ep;
    
    dp = opendir(path);

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if (strstr(ep->d_name, ".json")) jumlah_file++;
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    return jumlah_file;
}

char* randomFile(char path[]){
    int jumlah_file= jumlahFile(path);
    int random_value= rand()%jumlah_file;
    DIR *dp;
    struct dirent *ep;
    
    dp = opendir(path);

    if (dp != NULL)
    {
        int i=0;
        while ((ep = readdir (dp))) {
            if(i>=random_value-1 && strstr(ep->d_name, ".json")) return ep->d_name;
            i++;
        }

        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

char* getData(char path[]){
    FILE *fp;
    char buffer[4096];
    char *output = malloc (sizeof (char) * 1024);
    struct json_object *parsed_json;
	struct json_object *name;
    struct json_object *rarity;

    char filename[100];
    snprintf(filename, 100, "%s/%s", path, randomFile(path));

    fp = fopen(filename, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    snprintf(output, 100, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
    return output;
}

void gatcha(){
    char waktu[30], filename[100], foldername[100], message[100];
    int jumlah_gatcha = 1;
    int current_primogems = primogems_start;

    FILE * fptr;
    while(current_primogems > harga_gatcha){
    current_primogems-=harga_gatcha;
        if((jumlah_gatcha%90-1)==0){
            if(jumlah_gatcha > 1) chdir("..");
            snprintf(foldername, 100, "total_gatcha_%d", jumlah_gatcha);
            
            char *argv[] = {"mkdir", "-p", foldername, NULL};
            jalankan("/bin/mkdir", argv);
            chdir(foldername);
        }

        if((jumlah_gatcha%10-1)==0 || jumlah_gatcha==1){
            sleep(1);
            time_t t1 = time(NULL);
            struct tm* p1 = localtime(&t1);

            strftime(waktu, 30, "%H:%M:%S", p1);
            snprintf(filename, 100, "%s_gatcha_%d.txt", waktu, jumlah_gatcha);
        }

        if(jumlah_gatcha%2==0){
            fptr = fopen(filename, "a+");
            snprintf(message, 100, "%d_weapon_%s_%d\n", jumlah_gatcha, getData("../../weapons"), current_primogems);
            fputs(message, fptr);
            fclose(fptr);
        }else{
            fptr = fopen(filename, "a+");
            snprintf(message, 100, "%d_character_%s_%d\n", jumlah_gatcha, getData("../../characters"), current_primogems);
            fputs(message, fptr);
            fclose(fptr);
        }
        jumlah_gatcha++;
    }
    chdir("..");
}

void sembunyikan(){
    char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
    jalankan("/bin/zip", argv);

    char *argv2[] = {"rm", "-r", "gacha_gacha", "characters", "weapons", NULL};
    jalankan("/bin/rm", argv2);

    char *argv3[] = {"rm", "characters.zip", "weapons.zip", NULL};
    jalankan("/bin/rm", argv3);
}

void makedaemon()
{
	pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/ubuntu/praktikum_sisop/soal-shift-sisop-modul-2-ita04-2022/soal1")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main()
{
    // makedaemon();
    while(1)
    {
        srand(time(NULL));

        char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
        jalankan("/bin/mkdir", argv);
        
        download();
        chdir("gacha_gacha");
        gatcha();
        chdir("..");
        sleep(3600*3);
        sembunyikan();
        break;
    }
    return 0;
}
