#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

int getlist(char *kategori){
  FILE *file_kategori;
  DIR *dp_kategori;
  struct dirent *ep_kategori;
  char path[] = "/home/ubuntu/shift2/drakor";
  char dirpath[40], txtfile[20];
  snprintf(dirpath, 40, "%s/%s", path, kategori);
  dp_kategori = opendir(dirpath);
  while ((ep_kategori = readdir (dp_kategori))){
    snprintf(txtfile, 20, "temp/%s.txt", kategori); // action.txt
    file_kategori = fopen(txtfile, "a");
    fprintf(file_kategori, "%s\n", ep_kategori->d_name); // simpan list nama file utk diolah menggunakan awk
    fclose(file_kategori);
  }
  (void) closedir(dp_kategori);
}
int renamefile(char *kategori){
  pid_t pid;
  int status;
  FILE *f_kategori, *f_name;
  char dirpath[40], file_kategori[25], file_name[25], filename[100], png[50], name[40], filerename[100];
  snprintf(dirpath, 40, "/home/ubuntu/shift2/drakor/%s", kategori); // /home/ubuntu/shift2/drakor/action
  snprintf(file_kategori, 25, "temp/%s_sort.txt", kategori); // temp/action_sort.txt
  snprintf(file_name, 25, "temp/%s_name.txt", kategori); // temp/action_name.txt
  f_kategori = fopen(file_kategori, "r");
  f_name = fopen(file_name, "r");
  while(fgets(png, 50, f_kategori) != NULL){
    snprintf(filename, 100, "%s/%s", dirpath, png); // /home/ubuntu/shift2/drakor/the-k2;2016;action.png
    strtok(filename, "\n");
    fgets(name, 50, f_name);
    snprintf(filerename, 100, "%s/%s", dirpath, name); // /home/ubuntu/shift2/drakor/the-k2.png
    strtok(filerename, "\n");
    pid = fork();
    if(pid == 0){
      char *argv[] = {"mv", filename, filerename, NULL};
      execv("/usr/bin/mv", argv);               
    } else while((wait(&status)) > 0);
  }
  fclose(f_kategori);
  fclose(f_name);
}
int makedata(char *kategori){
  char path[] = "/home/ubuntu/shift2/drakor";
  char f_data[50], path_name[25], path_year[25], n[35], y[8];
  FILE *data, *name, *year;
  snprintf(f_data, 50, "%s/%s/data.txt", path, kategori); // /home/ubuntu/shift2/drakor/action/data.txt
  snprintf(path_name, 25, "temp/%s_name.txt", kategori); // temp/action_name.txt
  snprintf(path_year, 25, "temp/%s_year.txt", kategori); // temp/action_year.txt
  name = fopen(path_name, "r"); //forecasting-love-and-weather;2022;romance
  year = fopen(path_year, "r");
  data = fopen(f_data, "w");
  fprintf(data, "kategori : %s\n", kategori);
  while(fgets(n, 35, name) != NULL){
    if(fgets(y, 8, year) != NULL){
      fprintf(data, "\nnama : %srilis : %s", n, y);
    }
  }
  fclose(data);
  fclose(name);
  fclose(year);
}
int main(){
  char path[] = "/home/ubuntu/shift2/drakor";
  pid_t a1, a2, a3, b1, b2, b3d1, b3, d1, d2, d3c1, d4, d5, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11,
        c12, c13, c14, c15, c16e1, sort1, sort2, sort3, sort4, sort5, sort6, sort7, e2, e3, e4, e5, e6, e7;
  int status;
  a1 = fork();
  if(a1 == 0){
    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/usr/bin/mkdir", argv); // buat folder shift2/drakor
  } else{
    while((wait(&status)) > 0);
    a2 = fork();
    if(a2 == 0){
      char *argv[] = {"unzip",  "-q", "drakor.zip", "-d", path, NULL};
      execv("/usr/bin/unzip", argv); // unzip drakor.zip
    } else{
      while((wait(&status)) > 0);
      a3 = fork();
      if(a3 == 0){
        char *argv[] = {"find", path, "-not", "-name", "*.png", "-delete", NULL};
        execv("/usr/bin/find", argv); // hapus semua yang namanya tidak mengandung ".png"
      } else{
        while((wait(&status)) > 0);
        b1 = fork();
        if(b1 == 0){
          char *argv[] = {"mkdir", "-p", "temp", NULL};
          execv("/usr/bin/mkdir", argv); // buat folder temp untuk menyimpan hasil txt (nanti akan dihapus)
        } else{
          while((wait(&status)) > 0);
          b2 = fork();
          if(b2 == 0){
            FILE *fp;
            DIR *dp;
            struct dirent *ep;
            dp = opendir(path);
            while ((ep = readdir(dp))){
              fp = fopen("temp/1list_file.txt", "a");
              fprintf(fp, "%s\n", ep->d_name); // simpan list nama file utk diolah menggunakan awk
              fclose(fp);
            }
            (void) closedir(dp);
            char *argv[] = {"awk", "-F", "[;]", "{print $NF > \"temp/2kategori_png.txt\"}", "temp/1list_file.txt", NULL};
            execv("/usr/bin/awk", argv); // ambill [kategori].png
          } else{
            while((wait(&status)) > 0);
            b3d1 = fork();
            if(b3d1 == 0){
              char *argv[] = {"awk", "-F", "[.]", "{d_name[$1]++} END{for(name in d_name) print name > \"temp/3kategori.txt\"}", "temp/2kategori_png.txt", NULL};
              execv("/usr/bin/awk", argv); // ambil kategori
            } else{
              while((wait(&status)) > 0);
              d1 = fork();
              if(d1 == 0){
                char foldername[60], dir[10];
                FILE *file_dir ;
                file_dir = fopen("temp/3kategori.txt", "r");
                while(fgets(dir, 10, file_dir) != NULL){
                  snprintf(foldername, 60, "%s/%s", path, dir);
                  strtok(foldername, "\n");
                  b3 = fork();
                  if(b3 == 0){
                    char *argv[] = {"mkdir", "-p", foldername, NULL};
                    execv("/usr/bin/mkdir", argv); // buat folder sesuai nama yg ada dlm file txt
                  } else  while((wait(&status)) > 0);
                }
                fclose(file_dir);
                //d 
                char *argv[] = {"awk", "/_/ {print > \"temp/4png_double.txt\"}", "temp/1list_file.txt", NULL};
                execv("/usr/bin/awk", argv); // ambil file yang berisi 2 foto
              } else{
                while((wait(&status)) > 0);
                d2 = fork();
                if(d2 == 0){
                  char *argv[] = {"awk", "-F", "[_]", "{print $1\".png\" > \"temp/5png1.txt\"}", "temp/4png_double.txt", NULL};
                  execv("/usr/bin/awk", argv); // ambil nama file pertama
                } else{
                  while((wait(&status)) > 0);
                  d3c1 = fork();
                  if(d3c1 == 0){
                    char *argv[] = {"awk", "-F", "[_]", "{print $2 > \"temp/6png2.txt\"}", "temp/4png_double.txt", NULL};
                    execv("/usr/bin/awk", argv); // ambil nama file kedua
                  } else{
                    while((wait(&status)) > 0);
                    FILE *f_png_double, *f_png2, *f_png1;
                    char filename[90], filecopy[70], filepng1[70], pngname[60], png2[40], png1[40];
                    f_png_double = fopen("temp/4png_double.txt", "r");
                    f_png1 = fopen("temp/5png1.txt", "r");
                    f_png2 = fopen("temp/6png2.txt", "r");
                    while(fgets(pngname, 60, f_png_double) != NULL){
                      snprintf(filename, 90, "%s/%s", path, pngname); // /home/ubuntu/shift2/drakor/start-up;2020;romance_the-k2;2016;action.png
                      strtok(filename, "\n");
                      fgets(png2, 40, f_png2);
                      snprintf(filecopy, 70, "%s/%s", path, png2); // /home/ubuntu/shift2/drakor/the-k2;2016;action.png
                      strtok(filecopy, "\n");
                      fgets(png1, 40, f_png1);
                      snprintf(filepng1, 70, "%s/%s", path, png1); // /home/ubuntu/shift2/drakor/start-up;2020;romance.png
                      strtok(filepng1, "\n");
                      d4 = fork();
                      if(d4 == 0){
                        char *argv[] = {"cp", filename, filecopy, NULL};
                        execv("/usr/bin/cp", argv);                
                      } else{
                        while((wait(&status)) > 0);
                        d5 = fork();
                        if(d5 == 0){
                          char *argv[] = {"mv", filename, filepng1, NULL};
                          execv("/usr/bin/mv", argv);
                        } else  while((wait(&status)) > 0);
                      }
                    }
                    fclose(f_png_double);
                    fclose(f_png1);
                    fclose(f_png2);
                    //c
                    char filemove[290], fmove[300];
                    DIR *_dp;
                    struct dirent *_ep;
                    _dp = opendir(path);
                    while((_ep = readdir(_dp))){
                      snprintf(filemove, 290, "%s/%s", path, _ep->d_name);
                      strtok(filemove, "\n"); // filemove : /home/ubuntu/shift2/drakor/forecasting-love-and-weather;2022;romance.png
                      if(strstr(_ep->d_name, ".png")){
                        if      (strstr(_ep->d_name, "action"))     snprintf(fmove, 300, "%s/action/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "comedy"))     snprintf(fmove, 300, "%s/comedy/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "fantasy"))    snprintf(fmove, 300, "%s/fantasy/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "horror"))     snprintf(fmove, 300, "%s/horror/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "romance"))    snprintf(fmove, 300, "%s/romance/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "school"))     snprintf(fmove, 300, "%s/school/%s", path, _ep->d_name);
                        else if (strstr(_ep->d_name, "thriller"))   snprintf(fmove, 300, "%s/thriller/%s", path, _ep->d_name);
                        strtok(fmove, "\n"); // fmove : /home/ubuntu/shift2/drakor/romance/forecasting-love-and-weather;2022;romance.png
                        c1 = fork();
                        if(c1 == 0){
                          char *argv[] = {"mv", filemove, fmove, NULL};
                          execv("/usr/bin/mv", argv);
                        } else  while((wait(&status)) > 0);
                      }
                    }
                    (void) closedir(_dp);
                    getlist("action");
                    getlist("comedy");
                    getlist("fantasy");
                    getlist("horror");
                    getlist("romance");
                    getlist("school");
                    getlist("thriller");
                    c2 = fork();
                    if(c2 == 0){
                      char *argv[] = {"awk", "/png/ {print $1 > \"temp/action_png.txt\"}", "temp/action.txt", NULL};
                      execv("/usr/bin/awk", argv); // ambil nama film action (.png)
                    } else{
                      while((wait(&status)) > 0);
                      sort1 = fork();
                      if(sort1 == 0){
                        char *argv[] = {"sort", "-t", ";", "-nk2", "temp/action_png.txt", "-o", "temp/action_sort.txt", NULL};
                        execv("/usr/bin/sort", argv); // sort action
                      } else{
                          while((wait(&status)) > 0);
                          c3 = fork();
                          if(c3 == 0){
                            char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/action_name.txt\"}", "temp/action_sort.txt", NULL};
                            execv("/usr/bin/awk", argv); // ambil nama film action
                          } else{
                            while((wait(&status)) > 0);
                            c4 = fork();
                            if(c4 == 0){
                              char *argv[] = {"awk", "/png/ {print $1 > \"temp/comedy_png.txt\"}", "temp/comedy.txt", NULL};
                              execv("/usr/bin/awk", argv); // ambil nama film comedy (.png)
                            } else{
                              while((wait(&status)) > 0);
                              sort2 = fork();
                              if(sort2 == 0){
                                char *argv[] = {"sort", "-t", ";", "-nk2", "temp/comedy_png.txt", "-o", "temp/comedy_sort.txt", NULL};
                                execv("/usr/bin/sort", argv); // sort comedy
                              } else{
                                while((wait(&status)) > 0);
                                c5 = fork();
                                if(c5 == 0){
                                  char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/comedy_name.txt\"}", "temp/comedy_sort.txt", NULL};
                                  execv("/usr/bin/awk", argv); // ambil nama film comedy
                                } else{
                                  while((wait(&status)) > 0);
                                  c6 = fork();
                                  if(c6 == 0){
                                    char *argv[] = {"awk", "/png/ {print $1 > \"temp/fantasy_png.txt\"}", "temp/fantasy.txt", NULL};
                                    execv("/usr/bin/awk", argv); // ambil nama film fantasy (.png)
                                  } else{
                                    while((wait(&status)) > 0);
                                    sort3 = fork();
                                    if(sort3 == 0){
                                      char *argv[] = {"sort", "-t", ";", "-nk2", "temp/fantasy_png.txt", "-o", "temp/fantasy_sort.txt", NULL};
                                      execv("/usr/bin/sort", argv); // sort fantasy
                                    } else{
                                      while((wait(&status)) > 0);
                                      c7 = fork();
                                      if(c7 == 0){
                                        char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/fantasy_name.txt\"}", "temp/fantasy_sort.txt", NULL};
                                        execv("/usr/bin/awk", argv); // ambil nama film fantasy
                                      } else{
                                        while((wait(&status)) > 0);
                                        c8 = fork();
                                        if(c8 == 0){
                                          char *argv[] = {"awk", "/png/ {print $1 > \"temp/horror_png.txt\"}", "temp/horror.txt", NULL};
                                          execv("/usr/bin/awk", argv); // ambil nama film horror (.png)
                                        } else{
                                          while((wait(&status)) > 0);
                                          sort4 = fork();
                                          if(sort4 == 0){
                                            char *argv[] = {"sort", "-t", ";", "-nk2", "temp/horror_png.txt", "-o", "temp/horror_sort.txt", NULL};
                                            execv("/usr/bin/sort", argv); // sort horror
                                          } else{
                                            while((wait(&status)) > 0);
                                            c9 = fork();
                                            if(c9 == 0){
                                              char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/horror_name.txt\"}", "temp/horror_sort.txt", NULL};
                                              execv("/usr/bin/awk", argv); // ambil nama film horror
                                            } else{
                                              while((wait(&status)) > 0);
                                              c10 = fork();
                                              if(c10 == 0){
                                                char *argv[] = {"awk", "/png/ {print $1 > \"temp/romance_png.txt\"}", "temp/romance.txt", NULL};
                                                execv("/usr/bin/awk", argv); // ambil nama film romance (.png)
                                              } else{
                                                while((wait(&status)) > 0);
                                                sort5 = fork();
                                                if(sort5 == 0){
                                                  char *argv[] = {"sort", "-t", ";", "-nk2", "temp/romance_png.txt", "-o", "temp/romance_sort.txt", NULL};
                                                  execv("/usr/bin/sort", argv); // sort romance
                                                } else{
                                                  while((wait(&status)) > 0);
                                                  c11 = fork();
                                                  if(c11 == 0){
                                                    char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/romance_name.txt\"}", "temp/romance_sort.txt", NULL};
                                                    execv("/usr/bin/awk", argv); // ambil nama film romance
                                                  } else{
                                                    while((wait(&status)) > 0);
                                                    c12 = fork();
                                                    if(c12 == 0){
                                                      char *argv[] = {"awk", "/png/ {print $1 > \"temp/school_png.txt\"}", "temp/school.txt", NULL};
                                                      execv("/usr/bin/awk", argv); // ambil nama film school (.png)
                                                    } else{
                                                      while((wait(&status)) > 0);
                                                      sort6 = fork();
                                                      if(sort6 == 0){
                                                        char *argv[] = {"sort", "-t", ";", "-nk2", "temp/school_png.txt", "-o", "temp/school_sort.txt", NULL};
                                                        execv("/usr/bin/sort", argv); // sort school
                                                      } else{
                                                        while((wait(&status)) > 0);
                                                        c13 = fork();
                                                        if(c13 == 0){
                                                          char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/school_name.txt\"}", "temp/school_sort.txt", NULL};
                                                          execv("/usr/bin/awk", argv); // ambil nama film school
                                                        } else{
                                                          while((wait(&status)) > 0);
                                                          c14 = fork();
                                                          if(c14 == 0){
                                                            char *argv[] = {"awk", "/png/ {print $1 > \"temp/thriller_png.txt\"}", "temp/thriller.txt", NULL};
                                                            execv("/usr/bin/awk", argv); // ambil nama film thriller (.png)
                                                          } else{
                                                            while((wait(&status)) > 0);
                                                            sort7 = fork();
                                                            if(sort7 == 0){
                                                              char *argv[] = {"sort", "-t", ";", "-nk2", "temp/thriller_png.txt", "-o", "temp/thriller_sort.txt", NULL};
                                                              execv("/usr/bin/sort", argv); // sort thriller
                                                            } else{
                                                            while((wait(&status)) > 0);
                                                            c15 = fork();
                                                            if(c15 == 0){
                                                              char *argv[] = {"awk", "-F", "[;]", "/png/ {print $1 > \"temp/thriller_name.txt\"}", "temp/thriller_sort.txt", NULL};
                                                              execv("/usr/bin/awk", argv); // ambil nama film thriller
                                                            } else{
                                                              while((wait(&status)) > 0);
                                                              c16e1 = fork();
                                                              if(c16e1 == 0){
                                                                renamefile("action");
                                                                renamefile("comedy");
                                                                renamefile("fantasy");
                                                                renamefile("horror");
                                                                renamefile("romance");
                                                                renamefile("school");
                                                                renamefile("thriller");
                                                                //e
                                                                char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/action_year.txt\"}", "temp/action_sort.txt", NULL};
                                                                execv("/usr/bin/awk", argv); // ambil tahun film action
                                                              } else{
                                                                while((wait(&status)) > 0);
                                                                e2 = fork();
                                                                if(e2 == 0){
                                                                  char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/comedy_year.txt\"}", "temp/comedy_sort.txt", NULL};
                                                                  execv("/usr/bin/awk", argv); // ambil tahun film comedy
                                                                } else{
                                                                  while((wait(&status)) > 0);
                                                                  e3 = fork();
                                                                  if(e3 == 0){
                                                                    char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/fantasy_year.txt\"}", "temp/fantasy_sort.txt", NULL};
                                                                    execv("/usr/bin/awk", argv); // ambil tahun film fantasy
                                                                  } else{
                                                                    while((wait(&status)) > 0);
                                                                    e4 = fork();
                                                                    if(e4 == 0){
                                                                      char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/horror_year.txt\"}", "temp/horror_sort.txt", NULL};
                                                                      execv("/usr/bin/awk", argv); // ambil tahun film horror
                                                                    } else{
                                                                      while((wait(&status)) > 0);
                                                                      e5 = fork();
                                                                      if(e5 == 0){
                                                                        char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/romance_year.txt\"}", "temp/romance_sort.txt", NULL};
                                                                        execv("/usr/bin/awk", argv); // ambil tahun film romance
                                                                      } else{
                                                                        while((wait(&status)) > 0);
                                                                        e6 = fork();
                                                                        if(e6 == 0){
                                                                          char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/school_year.txt\"}", "temp/school_sort.txt", NULL};
                                                                          execv("/usr/bin/awk", argv); // ambil tahun film school
                                                                        } else{
                                                                          while((wait(&status)) > 0);
                                                                          e7 = fork();
                                                                          if(e7 == 0){
                                                                            char *argv[] = {"awk", "-F", "[;]", "/png/ {print $2 > \"temp/thriller_year.txt\"}", "temp/thriller_sort.txt", NULL};
                                                                            execv("/usr/bin/awk", argv); // ambil tahun film thriller
                                                                          } else{
                                                                            while((wait(&status)) > 0);
                                                                            makedata("action");
                                                                            makedata("comedy");
                                                                            makedata("fantasy");
                                                                            makedata("horror");
                                                                            makedata("romance");
                                                                            makedata("school");
                                                                            makedata("thriller");
                                                                            char *argv[] = {"rm", "-r", "temp", NULL};
                                                                            execv("/usr/bin/rm", argv); // hapus folder temp
                                                                          }
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}